terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "kienle"

    workspaces {
      name = "gitlab-ecs-terraform"
    }
  }
}
